import React, {useState} from "react";

const CarsList = () => {
    const [userPrice, setUserPrice] = useState('') // 유저가 입력하는 값
    const [error, seterror] = useState('') // 에러 메세지
    const [closestCarName, setClosestCarName] = useState('') // 추천하는 차의 이름을 제시한다.

    const cars = [
        {name:"캐스퍼", price:1200},
        {name:"소나타", price:2000},
        {name:"k5", price:3000},
        {name:"g70", price:5300},
        {name:"사이버트럭", price:8000},
    ] // 판매할 자동차 리스트

    const handleUserPriceChang = e =>{
        setUserPrice(e.target.value) //이벤트가 일어나면 바꿔치기 해라. 이게 1웨이
    }

    const calculateCar = () =>{ // 신천자가 값을 넣으면 로직을 실행한다.
        const carPrice = (userPrice - 370) / 1.07 // 최소값 이상인지 확인. 및 부가세 포함 가격
        let closestCal = cars[0] // 리스트 초기 값.
        let minDiff = Math.abs(cars[0].price - carPrice) // abs x의 절대값 반환 리스트의 price - 신청자가 작성한 값

        for (let i = 1; i < cars.length; i++){ //배열 전체를 정렬 하고 하나씩 검사.
            const diff = Math.abs(cars[i].price - carPrice)// 한번 거처간 리스트의 요소에 순번을 지정해준다.
            if (diff < minDiff && cars[i].price <= carPrice){ //검사하는 요소가 신청자가 입력한 값보다 가격이 크면 바로 전 요소를 불러온다.
                minDiff =diff
                closestCal = cars[i]// 리스트의 요소를 하나하나 들어 간다. 가격 이름 동시에 들어감
            }
        }

        if (closestCal.price > userPrice){
            seterror('돈이 부족해')// 최소 급액을 넘지 못하면 에러 메세지 리턴
        }else {
            setClosestCarName(closestCal.name)// 추천할 차의 이름을 제시한다.
        }
    }

    return(
        <div>
            <input type="number" value={userPrice} onChange={handleUserPriceChang}/>
            <button onClick={calculateCar}>추천받기</button>
            {error ? <p>{error}</p> : <p>추천차량 {closestCarName}</p>}
        </div>// 삼함연산자를 이용하여 참이면 에러메세지 거짓이면 추천차량이름 리턴
    )
}

export default CarsList